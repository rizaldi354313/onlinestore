$(document).ready(function() {
	category_list();
	category_slug();
});
function category_list() {
	$("#category").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
}
function category_slug() {
	$("input").attr('id', 'category_name').keyup(function(){
		var id = $(this).val();
		$("input").attr('id', 'category_slug').val(id);
		$("input").attr('id', 'category_slug_disabled').val(id);
    });
}