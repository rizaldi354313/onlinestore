<?php $this->load->view('include/breadcumb'); ?>

<div class="shop-details-area pb-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <div class="row">
                    <div class="col-md-6 padright col-12">
                        <div class="single-product-image product-image-slider fix">
                            <?php foreach ($product_detail->product_image as $product_image): ?>
                                <div class="p-thumb"><img src="<?php echo base_url('') ?>public/img/product/<?php echo $product_image->product_image; ?>" alt=""></div>
                            <?php endforeach ?>
                        </div>
                        <div class="single-product-thumbnail product-thumbnail-slider float-left">
                            <?php foreach ($product_detail->product_image as $product_image): ?>
                                <div class="p-thumb"><img src="<?php echo base_url('') ?>public/img/product/<?php echo $product_image->product_image; ?>" alt=""></div>
                            <?php endforeach ?>
                        </div>
                    </div>    
                    <div class="col-md-6">
                        <div class="details-content">
                            <h2 id="title_product"><?php echo $product_detail->title; ?></h2>
                            <!-- <div class="product-rating">
                                <i class="fa fa-star color"></i>
                                <i class="fa fa-star color"></i>
                                <i class="fa fa-star color"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            </div> -->
                            <p id="front_image" style="display: none;"><?php echo $product_detail->front_image; ?></p>
                            <p id="desc_product"><?php echo $product_detail->short_description; ?></p>
                            <div class="price-box">
                                <span id="price_products">
                                    <?php if ($this->session->userdata('format_currency') == "USD"): ?>
                                        US $<?php echo $this->Currency_Converter->convert('IDR', 'USD', $product_detail->sale_price, true, 1); ?>
                                    <?php else: ?>
                                       Rp <?php echo number_format($product_detail->sale_price,0,",","."); ?>
                                    <?php endif ?>
                                    <!-- <?php print_r($this->curl_request->currency_convert("USD", "IDR", 2000)); ?> -->
                                <span id="price_product" style="display: none;"><?php echo $product_detail->sale_price; ?>
                                </span>       
                            </div>
                            <div class="box-to-cart">
                                <div class="control">
                                    <input value="1" type="number" name="qty_product" id="qty_product">
                                    <input value="<?php echo $product_detail->product_id; ?>" type="hidden" name="product_id" id="product_id">
                                </div>
                                <a class="add" href="#" id="buy_now" class="buy_now">Buy Now</a>
                            </div>
                            <div class="add-to-link">
                                <ul>
                                    <!-- <li><a href="#"><i class="fa fa-retweet"></i>Add to compare</a></li> -->
                                </ul>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="tab-content-menu pt-80">
                    <div class="feature-tab feature-title">
                        <ul class="nav" role="tablist"> 
                            <li><a href="#tab1" class="active" data-toggle="tab" role="tab" aria-controls="tab1">details</a></li>
                            <!-- <li><a href="#tab2" aria-controls="tab2" aria-selected="false" role="tab" data-toggle="tab">reviews 1</a></li> -->
                        </ul> 
                    </div>
                </div>
                <div class="container">
                    <div class="custom-row">
                        <div class="tab-content">
                            <div class="tab-pane active show fade" id="tab1" role="tabpanel">
                                <p><?php echo $product_detail->description; ?></p>
                            </div>
                            <div class="tab-pane fade" id="tab2" role="tabpanel">
                                <p>Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</p>
                            </div>
                        </div> 
                    </div>  
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="shop-left-sidebar">
                    <div class="single-left-widget">
                        <div class="shop-banner">
                            <img src="<?php echo base_url('') ?>public/img/banner/shop-side.png" alt="shop">
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>